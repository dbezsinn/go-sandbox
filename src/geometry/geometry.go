package main

import (
	"fmt"
	"geometry/rectangle"
	"log"
)

// 1.Package variables
var rectLen, rectWid float64 = 12.25, 24.32

// 2. init function to check if the length and width are greater than zero
func init() {
	println("main package initialized")

	if rectLen <= 0 {
		log.Fatal("length must be greater than zero")
	}
	if rectWid <= 0 {
		log.Fatal("width must be greater than zero")
	}
}

func main() {
	fmt.Println("Geometrical shape properties")
	fmt.Printf("Area of rectangle: %.2f\n", rectangle.Area(rectLen, rectWid))
	fmt.Printf("Perimeter of rectangle: %.2f\n", rectangle.Perimeter(rectLen, rectWid))
}
