package rectangle

import (
	"fmt"
	"math"
)

func init() {
	fmt.Println("rectangle package initialized")
}

// Area - calculate rectangle area
func Area(len, wid float64) (area float64) {
	area = len * wid
	return
}

// Diagonal - calculate rectangle diagonal
func Diagonal(len, wid float64) (diagonal float64) {
	diagonal = math.Sqrt((len * len) + (wid * wid))
	return
}

// Perimeter - calculate rectangle perimeter
func Perimeter(len, wid float64) (perimeter float64) {
	perimeter = (len + wid) * 2
	return
}
