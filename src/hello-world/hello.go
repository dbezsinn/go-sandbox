package main

import (
	"fmt"
	"math"
	"strconv"
	"unsafe"
)

func main() {
	var width, height, length = 30, 40, 60
	name, age := "Dmytro", 28

	fmt.Println("My name is", name, "and I am", age)
	fmt.Println("The size of the luggage is:", length, "x", width, "x", height)

	fmt.Printf("Hello World\n")

	a, c := 12.33, 144.12
	max, min := math.Max(a, c), math.Min(a, c)
	fmt.Println(max, min)

	var aStaticInt int = 89
	var aStaticInt8 int8 = 115
	aInferredInt := 95
	fmt.Println("value of aStaticInt is", aStaticInt, "and aInferredInt", aInferredInt, "and aStaticInt8", aStaticInt8)
	fmt.Printf("type of aStaticInt is %T and size %d\n", aStaticInt, unsafe.Sizeof(aStaticInt))
	fmt.Printf("type of aInferredInt is %T and size %d\n", aInferredInt, unsafe.Sizeof(aInferredInt))
	fmt.Printf("type of aStaticInt8 is %T and size %d\n", aStaticInt8, unsafe.Sizeof(aStaticInt8))

	c1, c2 := complex(5, 7), 8+27i
	cadd, cmul := c1+c2, c1*c2
	fmt.Println("sum:", cadd)
	fmt.Println("mul:", cmul)

	secondName := "Bezsinnyi"
	fullName := secondName + " " + name
	fmt.Println(fullName)

	// Type conversions
	aInt, bFloat := 10, 12.35
	// resFloat := aInt + bFloat    won't work without type conversion
	resFloat := float64(aInt) + bFloat
	fmt.Println(resFloat)
	description := name + " " + secondName + " " + strconv.Itoa(age)
	fmt.Println(description)

	// Constants
	const constA = 5
	var intVarA int = constA
	var int32VarA int32 = constA
	var float64VarA float64 = constA
	var complex64VarA complex64 = constA
	fmt.Println("intVar", intVarA, "\nint32Var", int32VarA, "\nfloat64Var", float64VarA, "\ncomplex64Var", complex64VarA)
}
