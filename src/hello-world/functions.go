package main

import (
	"fmt"
)

func factorial(n int) int {
	var res = 1

	for i := n; i > 2; i-- {
		res *= i
	}

	return res
}

func rectangleProps(length, width float64) (area, perimeter float64) {
	// named return parameters
	area = length * width
	perimeter = (length + width) * 2

	return
	// no explicit return statement and variable declaration since they are declared in return parameters
}

func main() {
	res := factorial(10)
	fmt.Println(res)

	area, perimeter := rectangleProps(10.8, 5.6)
	fmt.Println("Area:", area, "Perimeter:", perimeter)

	area2, _ := rectangleProps(12.2, 33.5)
	fmt.Println("Area:", area2)
}
