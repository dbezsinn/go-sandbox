package main

import (
	"fmt"
	"loops/lmoperations"
	"os"
	"strconv"
)

func init() {
	println("main package initialized")
}

func main() {
	// var factorialOfTen = lmoperations.Factorial(10)

	// fmt.Println("!10 =", factorialOfTen)
	// fmt.Println("Is 223 prime number:", lmoperations.IsPrimeNumber(223))

	// fmt.Println("All primes less than 200")

	// primes := lmoperations.FindAllPrimesDummy(200)
	// fmt.Printf("len=%d cap=%d %v\n", len(primes), cap(primes), primes)

	cmdArgs := os.Args[1:]

	numberToFactorize, _ := strconv.ParseInt(cmdArgs[0], 10, 64)
	primeFactors := lmoperations.PrimeFactorize(numberToFactorize)
	fmt.Println("Factorization of", numberToFactorize)
	fmt.Printf("len=%d cap=%d %v\n", len(primeFactors), cap(primeFactors), primeFactors)
}
