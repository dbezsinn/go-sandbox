package lmoperations

import (
	"math"
)

func init() {
	println("loop-math-operations initialized")
}

// Factorial calculate factorial
func Factorial(n int) (result int64) {
	result = 1
	for i := 2; i <= n; i++ {
		result *= int64(i)
	}

	return
}

// IsPrimeNumber find out if the number is prime
func IsPrimeNumber(n int64) bool {
	if n%2 == 0 {
		return false
	}

	var i int64

	nSqrt := math.Sqrt(float64(n))
	for i = 3; float64(i) <= nSqrt; i += 2 {
		if n%i == 0 {
			return false
		}
	}

	return true
}

// FindAllPrimesDummy dummy algorithm to find all prime numbers which are strictly less than [n]
func FindAllPrimesDummy(n int64) []int64 {
	var res []int64

	if n > 2 {
		res = append(res, 2)
	}

	var i int64
	for i = 3; i < n; i += 2 {
		if IsPrimeNumber(i) {
			res = append(res, i)
		}
	}

	return res
}

// PrimeFactorize prime factorize number
func PrimeFactorize(n int64) []int64 {
	primes := FindAllPrimesDummy(n + 1)
	factorized := n
	var res []int64

	for factorized != 1 {
		for i := 0; i < len(primes); i++ {
			if factorized%primes[i] == 0 {
				res = append(res, primes[i])
				factorized = factorized / primes[i]
				break
			}
		}
	}

	return res
}
